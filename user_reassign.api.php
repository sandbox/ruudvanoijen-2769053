<?php

/**
 * @file
 * User reassign API document.
 */

/**
 * This is an example function to demonstrate a user reassign uid.
 *
 * @param $username
 *   The username of the autocomplete field.
 * @param $uid
 *   The uid of the user that is receiving the new content.
 */
function hook_user_reassign_uid_alter(&$username, &$uid) {
  // Get user uid out of the string.
  preg_match_all("/\[(.*)\]/", $username, $uid);
  $uid = (int) $uid[1][0];
}

/**
 * This is an example function to demonstrate a user reassign validation.
 *
 * @param $autocomplete
 *   The custom autocomplete function you have created.
 * @param $form
 *   The form object where you can set your own validation.
 */
function hook_user_reassign_validate_alter(&$autocomplete, &$form) {
  $autocomplete = 'ldap_user/autocomplete';
  $form['#validate'][] = 'user_reassign_ldap_confirm_form_validate';
}
