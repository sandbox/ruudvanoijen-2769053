INTRODUCTION
------------

Module for reassigning content from one user to another.
Instead of doing this through a views bulk operation.
This will fit better in a process of removing a user.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/ruudvanoijen/2769053

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2769053

INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will prevent options from the user cancel form from appearing.
To get the options back, disable the module and clear caches.

MAINTAINERS
-----------

Current maintainers:
 * Ruud Van Oijen (ruudvanoijen) - https://www.drupal.org/u/ruudvanoijen
 * Albert Skibinski (askibinski) - https://www.drupal.org/u/askibinski
 * Martijn van Wensen (mvwensen) - https://www.drupal.org/u/mvwensen
 * Ralph van den Houdt (ralphvdhoudt) - https://www.drupal.org/u/ralphvdhoudt

This project has been sponsored by:
* 	ezCompany - https://www.drupal.org/ezcompany
	ezCompany was founded in 2001 in the Netherlands and we are fully focussed on Drupal development. Our business philosophy emphasizes the Agile Manifesto and we use Scrum throughout our organization. We love to build easy, functional and high traffic web-based applications with our 20 dedicated employees.
	Our clients include several Forbes 2000 companies such as Philips, Unilever and Ziggo. We also serve the Dutch government and are co-initiator of a Distribution for Municipalities which is used by for instance Vught.nl and Almelo.nl
